
#include <iostream>
#include <cassert>
#include "test.cpp"

using namespace std;

class ListTester {
public:
	void testGetLast();
};

void ListTester::testGetLast() {
	cout << "-testing getLast()..." << flush;
	List<double> aList;
	assert( aList.getSize() == 0 );

	aList.prepend(33);
	assert( aList.getSize() == 1 );
	assert( aList.getFirst() == 33 );               // line numbers:
	assert( aList.getLast() == 33 );                // 16
	cout << " 0 " << flush;

	aList.prepend(22);
	assert( aList.getSize() == 2 );
	assert( aList.getFirst() == 22 );
	assert( aList.getLast() == 33 );                // 22
	cout << " 1 " << flush;

	aList.prepend(11);
	assert( aList.getSize() == 3 );
	assert( aList.getFirst() == 11 );
	assert( aList.getLast() == 33 );                // 28
	assert( aList.myFirst->myNext->myItem == 22 );
	cout << " 2 " << flush;
	cout << " Passed! " << endl;
}

int main() {
	ListTester x;
	x.testGetLast();
}
